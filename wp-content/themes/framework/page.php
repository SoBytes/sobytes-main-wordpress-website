<?php get_header(); ?>
<!-- Start the Loop. -->
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); 
		$video_url = get_post_meta( get_the_ID(), 'video', true );
		?>
	<!-- ==== SECTION DIVIDER2 -->
	<section class="section-divider textdivider divider1" style="background-image: url('http://sobytes.com/wp-content/uploads/2015/02/divider7.png');">
		<div class="container">
			<h1><?php echo strtoupper(get_the_title()); ?></h1>
			<hr/>
		</div><!-- container -->
	</section><!-- section -->
	<!-- ==== SERVICES ==== -->
	<div class="container" id="services" name="services">
		<br>
		<br>
		<div class="row">
				<div class="col-lg-offset-2 col-lg-8 blog-content-single">
					<?php the_content(); ?>
				</div><!-- col-lg -->
		</div><!-- row -->
		<div class="row">
			<hr>
			<br>
			<div class="col-lg-offset-2 col-lg-8">
				<?php
				if($video_url){ ?>
					<iframe style="width:100%;" onload="this.height=(this.offsetWidth/16)*9;" src="https://www.youtube.com/embed/C3fvqOmuohk" frameborder="0" allowfullscreen></iframe>
				<?php }else{ 
					if ( has_post_thumbnail() ) {  ?>
						<img title="image title" alt="<?php echo get_the_title(); ?>" class="img-responsive" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>" style="margin: auto;"/>
					<?php }else{
						echo 'Display image';
					}
				}
				?>
			</div><!-- col -->
		</div><!-- row -->
	   <?php endwhile; else : ?>
			 <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
		<?php endif; ?>			
	</div><!-- container -->
	<!-- /container -->
<?php get_footer(); ?>