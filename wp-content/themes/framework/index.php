<?php get_header(); ?>
<!-- ==== SECTION DIVIDER2 -->
	<section class="section-divider textdivider divider3">
		<div class="container">
			<h1>SOBYTES BLOG</h1>
			<hr>
			<p>Follow our blog and read more about our latest projects</p>
		</div><!-- container -->
	</section><!-- section -->
	<!-- ==== SERVICES ==== -->
	<div class="container" id="services" name="services">
		<br>
		<br>
		<div class="row">
		<!-- Start the Loop. -->
		<div class="col-lg-9">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); 
			$shortexcerpt = substr(strip_tags(get_the_content(), '<p><a>'), 0 , 400);
			$author = get_the_author(); 
			$cat = get_the_category( $post->ID );
			$my_date = get_the_date('M d',$post->ID); 
			//$shortexcerpt = wp_trim_words( get_the_content(), $num_words = 120, $more = '… ' );
			?>
				<div class="col-lg-4 centered">
				<br>
					<p><?php echo get_wp_user_avatar(get_the_author_meta('ID'), 120); ?></p>
					<h4><?php echo $author ?></h4>
					<h5><?php echo $cat[0]->name; ?></h5>
				</div>
				<div class="col-lg-8 blog-content-index">
					<h2><a href="<?php the_permalink(); ?>"><?php echo strtoupper(get_the_title()); ?></a></h2>
					<?php echo $shortexcerpt; ?>
					<p><a href="<?php the_permalink(); ?>" class="icon icon-link"> Read More</a></p>
					<br>
				</div>
			
	   <?php endwhile; else : ?>
			 <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
		<?php endif; ?>
		<?php wp_pagenavi(); ?>
		</div>
		<div class="col-lg-3">
			<?php get_sidebar(); ?>
		</div>
		</div><!-- row -->			
	</div>
<?php get_footer(); ?>