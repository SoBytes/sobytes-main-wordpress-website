	<!-- ==== SECTION DIVIDER6 ==== -->
	<section class="section-divider textdivider divider6" id="contact" style="background-image: url('http://sobytes.com/wp-content/uploads/2015/02/map2.png');">
		<div class="container">
			<h1>REPRESENTING SOUTH WALES.</h1>
			<hr>
			<p>Britannia House, Caerphilly Business Park, Van Road, Caerphilly CF83 3GG</p>
			<p>0330 122 2365</p>
			<p><a style="color: white;" href="mailto:contact@sobytes.com">contact@sobytes.com</a></p>
			<p><a class="icon icon-twitter" href="https://twitter.com/sobytes" target="_blank"></a> | <a class="icon icon-facebook" href="https://www.facebook.com/Sobytes" target="_blank"></a></p>
		</div><!-- container -->
	</section><!-- section -->
	<div id="footerwrap">
		<div class="container">
			<h4>SoBytes Limited Copyright <?php echo date("Y"); ?> - Company Number 08266291</h4>
		</div>
	</div>

	<!-- mega menu -->
	</main>

	<div class="sobytes-menu-overlay"></div>

	<nav class="sobytes-menu-nav">

		<?php
			if(function_exists('wp_nav_menu')) {
				wp_nav_menu( array(
				  'menu' => 'top_menu',
				  'depth' => 2,
				  'container' => false,
				  'menu_class' => 'sobytes-menu-primary-nav is-fixed',
				  'menu_id' => 'sobytes-menu-primary-nav',
				  //Process nav menu using our custom nav walker
				  'walker' => new wp_bootstrap_navwalker())
				);
			} else {
			 	wp_nav_menu( array(
				  'menu' => 'top_menu',
				  'depth' => 2,
				  'container' => false,
				  'menu_class' => 'sobytes-menu-primary-nav is-fixed',
				  'menu_id' => 'sobytes-menu-primary-nav',
				  //Process nav menu using our custom nav walker
				  'walker' => new wp_bootstrap_navwalker())
				);
			}
  		?><!-- /.nav -->

	</nav> <!-- sobytes-menu-nav -->

	<div id="sobytes-menu-search" class="sobytes-menu-search">
		<form class="search" method="get" action="<?php echo home_url(); ?>" role="search">
                <input type="search" name="s" placeholder="Search...">
        </form>
	</div>
	<!--/mega menu-->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->	
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/dist/js/jquery.mobile.custom.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/retina.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.easing.1.3.min.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/smoothscroll.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/slick/slick.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/dist/js/slider.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/dist/js/menu.min.js"></script>
	<?php wp_footer(); ?>
</body>
</html>