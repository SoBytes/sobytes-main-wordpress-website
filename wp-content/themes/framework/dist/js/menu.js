jQuery(document).ready(function($){
	//if you change this breakpoint in the style.css file (or _layout.scss if you use SASS), don't forget to update this value as well
	var MqL = 1170;
	//move nav element position according to window width
	moveNavigation();
	$(window).on('resize', function(){
		(!window.requestAnimationFrame) ? setTimeout(moveNavigation, 300) : window.requestAnimationFrame(moveNavigation);
	});

	//mobile - open lateral menu clicking on the menu icon
	$('.sobytes-menu-nav-trigger').on('click', function(event){
		event.preventDefault();
		if( $('.sobytes-menu-main-content').hasClass('nav-is-visible') ) {
			closeNav();
			$('.sobytes-menu-overlay').removeClass('is-visible');
		} else {
			$(this).addClass('nav-is-visible');
			$('.sobytes-menu-primary-nav').addClass('nav-is-visible');
			$('.sobytes-menu-main-header').addClass('nav-is-visible');
			$('.sobytes-menu-main-content').addClass('nav-is-visible').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
				$('body').addClass('overflow-hidden');
			});
			toggleSearch('close');
			$('.sobytes-menu-overlay').addClass('is-visible');
		}
	});

	//open search form
	$('.sobytes-menu-search-trigger').on('click', function(event){
		event.preventDefault();
		toggleSearch();
		closeNav();
	});

	//close lateral menu on mobile 
	$('.sobytes-menu-overlay').on('swiperight', function(){
		if($('.sobytes-menu-primary-nav').hasClass('nav-is-visible')) {
			closeNav();
			$('.sobytes-menu-overlay').removeClass('is-visible');
		}
	});
	$('.nav-on-left .sobytes-menu-overlay').on('swipeleft', function(){
		if($('.sobytes-menu-primary-nav').hasClass('nav-is-visible')) {
			closeNav();
			$('.sobytes-menu-overlay').removeClass('is-visible');
		}
	});
	$('.sobytes-menu-overlay').on('click', function(){
		closeNav();
		toggleSearch('close')
		$('.sobytes-menu-overlay').removeClass('is-visible');
	});


	//prevent default clicking on direct children of .sobytes-menu-primary-nav 
	$('.sobytes-menu-primary-nav').children('.has-children').children('a').on('click', function(event){
		event.preventDefault();
	});
	//open submenu
	$('.has-children').children('a').on('click', function(event){
		if( !checkWindowWidth() ) event.preventDefault();
		var selected = $(this);
		if( selected.next('ul').hasClass('is-hidden') ) {
			//desktop version only
			selected.addClass('selected').next('ul').removeClass('is-hidden').end().parent('.has-children').parent('ul').addClass('moves-out');
			selected.parent('.has-children').siblings('.has-children').children('ul').addClass('is-hidden').end().children('a').removeClass('selected');
			$('.sobytes-menu-overlay').addClass('is-visible');
		} else {
			selected.removeClass('selected').next('ul').addClass('is-hidden').end().parent('.has-children').parent('ul').removeClass('moves-out');
			$('.sobytes-menu-overlay').removeClass('is-visible');
		}
		toggleSearch('close');
	});

	//submenu items - go back link
	$('.go-back').on('click', function(){
		$(this).parent('ul').addClass('is-hidden').parent('.has-children').parent('ul').removeClass('moves-out');
	});

	function closeNav() {
		$('.sobytes-menu-nav-trigger').removeClass('nav-is-visible');
		$('.sobytes-menu-main-header').removeClass('nav-is-visible');
		$('.sobytes-menu-primary-nav').removeClass('nav-is-visible');
		$('.has-children ul').addClass('is-hidden');
		$('.has-children a').removeClass('selected');
		$('.moves-out').removeClass('moves-out');
		$('.sobytes-menu-main-content').removeClass('nav-is-visible').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
			$('body').removeClass('overflow-hidden');
		});
	}

	function toggleSearch(type) {
		if(type=="close") {
			//close serach 
			$('.sobytes-menu-search').removeClass('is-visible');
			$('.sobytes-menu-search-trigger').removeClass('search-is-visible');
			$('.sobytes-menu-overlay').removeClass('search-is-visible');
		} else {
			//toggle search visibility
			$('.sobytes-menu-search').toggleClass('is-visible');
			$('.sobytes-menu-search-trigger').toggleClass('search-is-visible');
			$('.sobytes-menu-overlay').toggleClass('search-is-visible');
			if($(window).width() > MqL && $('.sobytes-menu-search').hasClass('is-visible')) $('.sobytes-menu-search').find('input[type="search"]').focus();
			($('.sobytes-menu-search').hasClass('is-visible')) ? $('.sobytes-menu-overlay').addClass('is-visible') : $('.sobytes-menu-overlay').removeClass('is-visible') ;
		}
	}

	function checkWindowWidth() {
		//check window width (scrollbar included)
		var e = window, 
            a = 'inner';
        if (!('innerWidth' in window )) {
            a = 'client';
            e = document.documentElement || document.body;
        }
        if ( e[ a+'Width' ] >= MqL ) {
			return true;
		} else {
			return false;
		}
	}

	function moveNavigation(){
		var navigation = $('.sobytes-menu-nav');
  		var desktop = checkWindowWidth();
        if ( desktop ) {
			navigation.detach();
			navigation.insertBefore('.sobytes-menu-header-buttons');
		} else {
			navigation.detach();
			navigation.insertAfter('.sobytes-menu-main-content');
		}
	}
});