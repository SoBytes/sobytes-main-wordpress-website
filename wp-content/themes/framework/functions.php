<?php

/*-----------------------------------------------------------------------------------

	Here we have all the custom functions for the theme.
	Please be extremely cautious editing this file,
	When things go wrong, they tend to go wrong in a big way.
	You have been warned!

-------------------------------------------------------------------------------------*/


/*-----------------------------------------------------------------------------------*/
/*	Theme set up
/*-----------------------------------------------------------------------------------*/

if (!function_exists('rm_theme_setup')) {
    function rm_theme_setup() {	
    	/* Configure WP 2.9+ Thumbnails ---------------------------------------------*/
    	add_theme_support('post-thumbnails');
		add_image_size( 'post-image', 800, 400 );
		add_image_size( 'work-image', 355, 237, true );
		add_image_size( 'feat-image', 740, 420, true );
		add_image_size( 'team-image', 120, 120, true );
        add_image_size( 'team-home-image', 260, 250, true );      
    }
}

add_action('after_setup_theme', 'rm_theme_setup');

/*-----------------------------------------------------------------------------------*/
/*	Setup Theme menu
/*-----------------------------------------------------------------------------------*/
add_action( 'after_setup_theme', 'main_nav_setup' );
if ( ! function_exists( 'main_nav_setup' ) ){
    function main_nav_setup() {  
            register_nav_menu( 'primary', __( 'Primary navigation', 'SoBytes' ) );
	}
}

add_filter('show_admin_bar', '__return_false');



/*-----------------------------------------------------------------------------------*/
/*	Register Sidebars
/*-----------------------------------------------------------------------------------*/

if (!function_exists( 'rm_sidebars_init')) {
    function rm_sidebars_init() {
    	register_sidebar(array(
    		'name' => __('Main Sidebar', 'rockability'),
    		'id' => 'sidebar-main',
    		'before_widget' => '<div id="%1$s" class="widget %2$s">',
    		'after_widget' => '</div>',
    		'before_title' => '<h3 class="widget-title">',
    		'after_title' => '</h3>',
    	));
		register_sidebar(array(
    		'name' => __('Single Sidebar', 'rockability'),
    		'id' => 'single-main',
    		'before_widget' => '<div id="%1$s" class="widget %2$s">',
    		'after_widget' => '</div>',
    		'before_title' => '<h3 class="widget-title">',
    		'after_title' => '</h3>',
    	));
	}
}

add_action('widgets_init', 'rm_sidebars_init');


/*-----------------------------------------------------------------------------------*/
/*	Change Default Excerpt Length
/*-----------------------------------------------------------------------------------*/

if (!function_exists('rm_excerpt_length')) {
	function rm_excerpt_length($length) {
		return 55; 
	}
}

add_filter('excerpt_length', 'rm_excerpt_length');


/*-----------------------------------------------------------------------------------*/
/*	Configure Excerpt String
/*-----------------------------------------------------------------------------------*/

if (!function_exists('rm_excerpt_more')) {
	function rm_excerpt_more($excerpt) {
		return str_replace('[...]', '...', $excerpt); 
	}
}

add_filter('wp_trim_excerpt', 'rm_excerpt_more');


/*-----------------------------------------------------------------------------------*/
/*	Custom More Link Output
/*-----------------------------------------------------------------------------------*/

if (!function_exists('rm_custom_more_link')) {
    function rm_custom_more_link($more_link, $more_link_text) {
        return str_replace($more_link_text, "<span>$more_link_text</span>", $more_link);
   }
}
add_filter('the_content_more_link', 'rm_custom_more_link', 10, 2);


/*-----------------------------------------------------------------------------------*/
/*	Configure Default Title
/*-----------------------------------------------------------------------------------*/

if (!function_exists('rm_wp_title')) {
	function rm_wp_title($title) {
		if( is_front_page() ){
			return get_bloginfo('name') .' | '. get_bloginfo('description'); 
		} else {
			return trim($title) .' | '. get_bloginfo('name'); 
		}
		return $title;
	}
}
add_filter('wp_title', 'rm_wp_title');


/*-----------------------------------------------------------------------------------*/
/*	Register javascript and css
/*-----------------------------------------------------------------------------------*/

if (!function_exists('rm_enqueue_scripts')) {
	function rm_enqueue_scripts() {
	    /* Register scripts -----------------------------------------------------*/
		/*wp_deregister_script('jquery');
		wp_register_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js');
		wp_register_script('validation', 'http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js', 'jquery');
        wp_register_script('bootstrap', get_bloginfo('template_directory'). '/asset/js/bootstrap.min.js', 'jquery');*/

		/* Enqueue scripts ------------------------------------------------------*/
		//wp_enqueue_script('jquery');
		
		//if(is_singular()) wp_enqueue_script('comment-reply'); // loads the javascript required for threaded comments 
		//if(is_page_template('template-contact.php')) wp_enqueue_script('validation');

        /* Register styles -----------------------------------------------------*/
        /*wp_register_style('bootstrap', get_bloginfo('template_directory'). '/css/bootstrap.min.css', 'jquery');
        wp_register_style('bootstrap_responsive', get_bloginfo('template_directory'). '/css/bootstrap-responsive.min.css', 'jquery');
        wp_register_style('main_styles', get_bloginfo('stylesheet_url'), 'jquery');*/

        /* Enqueue styles ------------------------------------------------------*/
        //wp_enqueue_style('bootstrap');
        //wp_enqueue_style('bootstrap_responsive');
        //wp_enqueue_style('main_styles');
	}
}

add_action('wp_enqueue_scripts', 'rm_enqueue_scripts');


/*-----------------------------------------------------------------------------------*/
/*	Register admin javascript and css
/*-----------------------------------------------------------------------------------*/

if ( !function_exists('rockability_enqueue_admin_scripts')) {
    function rockability_enqueue_admin_scripts() {}
}

add_action( 'admin_enqueue_scripts', 'rockability_enqueue_admin_scripts' );


/*-----------------------------------------------------------------------------------*/
/*	Comment Styling
/*-----------------------------------------------------------------------------------*/

if (!function_exists( 'rockability_comment')) {
	function rockability_comment($comment, $args, $depth) {
	
        $GLOBALS['comment'] = $comment; ?>
        <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">

            <div id="comment-<?php comment_ID(); ?>">
                <span class="avatar"><?php echo get_avatar($comment,$size='40'); ?></span>
                <div class="comment-author vcard">
                    <?php printf(__('<cite class="fn">%s</cite>', 'rockability'), get_comment_author_link()) ?>
                </div>

                    <div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php printf(__('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)', 'rockability'),'  ','') ?> &middot; <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?></div>

                <?php if ($comment->comment_approved == '0') { ?>
                    <em class="moderation"><?php _e('Your comment is awaiting moderation.', 'rockability') ?></em>
                    <br />
                <?php } ?>

                <div class="comment-body">
                <?php comment_text() ?>
                </div>
            </div>
	<?php
	}
}


/*-----------------------------------------------------------------------------------*/
/*	Seperated Pings Styling
/*-----------------------------------------------------------------------------------*/

if (!function_exists('rockability_list_pings')) {
	function rockability_list_pings($comment, $args, $depth) {
	    $GLOBALS['comment'] = $comment; ?>
		<li id="comment-<?php comment_ID(); ?>"><?php comment_author_link(); ?>
		<?php 
	}
}


/*-----------------------------------------------------------------------------------*/
/*	Output Audio
/* 
/*  @param int $postid the post id
/*  @param int $width the width of the audio player
/*-----------------------------------------------------------------------------------*/

if (!function_exists('rockability_audio')) {
    function rockability_audio($postid, $width = 560) {
    	$mp3 = get_post_meta($postid, '_rockability_audio_mp3', TRUE);
    	$ogg = get_post_meta($postid, '_rockability_audio_ogg', TRUE);
    	$poster = get_post_meta($postid, '_rockability_audio_poster_url', TRUE);
    	$height = get_post_meta($postid, '_rockability_audio_height', TRUE);
    	$height = ($height) ? $height : 75;
    ?>

    		<script type="text/javascript">
    			jQuery(document).ready(function($){
    				if($().jPlayer) {
    					$("#jquery_jplayer_<?php echo $postid; ?>").jPlayer({
    						ready: function () {
    							$(this).jPlayer("setMedia", {
    							    <?php if($poster != '') : ?>
    							    poster: "<?php echo $poster; ?>",
    							    <?php endif; ?>
    							    <?php if($mp3 != '') : ?>
    								mp3: "<?php echo $mp3; ?>",
    								<?php endif; ?>
    								<?php if($ogg != '') : ?>
    								oga: "<?php echo $ogg; ?>",
    								<?php endif; ?>
    								end: ""
    							});
    						},
    						size: {
            				    width: "<?php echo $width; ?>px",
            				    height: "<?php echo $height . 'px'; ?>"
            				},
    						swfPath: "<?php echo get_template_directory_uri(); ?>/js",
    						cssSelectorAncestor: "#jp_interface_<?php echo $postid; ?>",
    						supplied: "<?php if($ogg != '') : ?>oga,<?php endif; ?><?php if($mp3 != '') : ?>mp3, <?php endif; ?> all"
    					});
					    
					    $('#jquery_jplayer_<?php echo $postid; ?>').bind($.jPlayer.event.playing, function(event) {
            			    $(this).add('#jp_interface_<?php echo $postid; ?>').hover( function() {
            			        $('#jp_interface_<?php echo $postid; ?>').stop().animate({ opacity: 1 }, 400);
        			        }, function() {
        			            $('#jp_interface_<?php echo $postid; ?>').stop().animate({ opacity: 0 }, 400);
        			        });
            			});

            			$('#jquery_jplayer_<?php echo $postid; ?>').bind($.jPlayer.event.pause, function(event) {
            			    $('#jquery_jplayer_<?php echo $postid; ?>').add('#jp_interface_<?php echo $postid; ?>').unbind('hover');

            			    $('#jp_interface_<?php echo $postid; ?>').stop().animate({ opacity: 1 }, 400);

            			});
    				}
    			});
    		</script>
		
    	    <div id="jquery_jplayer_<?php echo $postid; ?>" class="jp-jplayer jp-jplayer-audio"></div>

            <div class="jp-audio-container">
                <div class="jp-audio">
                    <div class="jp-type-single">
                        <div id="jp_interface_<?php echo $postid; ?>" class="jp-interface">
                            <ul class="jp-controls">
                            	<li><div class="seperator-first"></div></li>
                                <li><div class="seperator-second"></div></li>
                                <li><a href="#" class="jp-play" tabindex="1">play</a></li>
                                <li><a href="#" class="jp-pause" tabindex="1">pause</a></li>
                                <li><a href="#" class="jp-mute" tabindex="1">mute</a></li>
                                <li><a href="#" class="jp-unmute" tabindex="1">unmute</a></li>
                            </ul>
                            <div class="jp-progress-container">
                                <div class="jp-progress">
                                    <div class="jp-seek-bar">
                                        <div class="jp-play-bar"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="jp-volume-bar-container">
                                <div class="jp-volume-bar">
                                    <div class="jp-volume-bar-value"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    	<?php 
    }
}


/*-----------------------------------------------------------------------------------*/
/*  Output video
/*
/*  @param int $postid the post id
/*  @param int $width the width of the video player
/*-----------------------------------------------------------------------------------*/

if (!function_exists('rockability_video')) {
    function rockability_video($postid, $width = 560) {
    	$height = get_post_meta($postid, '_rockability_video_height', true);
    	$height = ($height) ? $height : 435;
    	$m4v = get_post_meta($postid, '_rockability_video_m4v', true);
    	$ogv = get_post_meta($postid, '_rockability_video_ogv', true);
    	$poster = get_post_meta($postid, '_rockability_video_poster_url', true);
	
    ?>
    <script type="text/javascript">
    	jQuery(document).ready(function($){
		
    		if($().jPlayer) {
    			$("#jquery_jplayer_<?php echo $postid; ?>").jPlayer({
    				ready: function () {
    					$(this).jPlayer("setMedia", {
    						<?php if($m4v != '') : ?>
    						m4v: "<?php echo $m4v; ?>",
    						<?php endif; ?>
    						<?php if($ogv != '') : ?>
    						ogv: "<?php echo $ogv; ?>",
    						<?php endif; ?>
    						<?php if ($poster != '') : ?>
    						poster: "<?php echo $poster; ?>"
    						<?php endif; ?>
    					});
    				},
    				size: {
    				    width: "<?php echo $width ?>px",
    				    height: "<?php echo $height . 'px'; ?>"
    				},
    				swfPath: "<?php echo get_template_directory_uri(); ?>/js",
    				cssSelectorAncestor: "#jp_interface_<?php echo $postid; ?>",
    				supplied: "<?php if($m4v != '') : ?>m4v, <?php endif; ?><?php if($ogv != '') : ?>ogv, <?php endif; ?> all"
    			});
    			
    			$('#jquery_jplayer_<?php echo $postid; ?>').bind($.jPlayer.event.playing, function(event) {
    			    $(this).add('#jp_interface_<?php echo $postid; ?>').hover( function() {
    			        $('#jp_interface_<?php echo $postid; ?>').stop().animate({ opacity: 1 }, 400);
			        }, function() {
			            $('#jp_interface_<?php echo $postid; ?>').stop().animate({ opacity: 0 }, 400);
			        });
    			});
    			
    			$('#jquery_jplayer_<?php echo $postid; ?>').bind($.jPlayer.event.pause, function(event) {
    			    $('#jquery_jplayer_<?php echo $postid; ?>').add('#jp_interface_<?php echo $postid; ?>').unbind('hover');
    			    
    			    $('#jp_interface_<?php echo $postid; ?>').stop().animate({ opacity: 1 }, 400);
			        
    			});
    		}
    	});
    </script>

    <div id="jquery_jplayer_<?php echo $postid; ?>" class="jp-jplayer jp-jplayer-video"></div>

    <div class="jp-video-container">
        <div class="jp-video">
            <div class="jp-type-single">
                <div id="jp_interface_<?php echo $postid; ?>" class="jp-interface">
                    <ul class="jp-controls">
                    	<li><div class="seperator-first"></div></li>
                        <li><div class="seperator-second"></div></li>
                        <li><a href="#" class="jp-play" tabindex="1">play</a></li>
                        <li><a href="#" class="jp-pause" tabindex="1">pause</a></li>
                        <li><a href="#" class="jp-mute" tabindex="1">mute</a></li>
                        <li><a href="#" class="jp-unmute" tabindex="1">unmute</a></li>
                    </ul>
                    <div class="jp-progress-container">
                        <div class="jp-progress">
                            <div class="jp-seek-bar">
                                <div class="jp-play-bar"></div>
                            </div>
                        </div>
                    </div>
                    <div class="jp-volume-bar-container">
                        <div class="jp-volume-bar">
                            <div class="jp-volume-bar-value"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php }
}

/*
 * Breadcrumb functions
 * @author sameast
 */
function get_breadcrumb() {

	global $post;

	$trail = '';
	$page_title = get_the_title($post->ID);

	if($post->post_parent) {
		$parent_id = $post->post_parent;

		while ($parent_id) {
			$page = get_page($parent_id);
			$breadcrumbs[] = '<li><a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a></li>';
			$parent_id = $page->post_parent;
		}

		$breadcrumbs = array_reverse($breadcrumbs);
		foreach($breadcrumbs as $crumb) $trail .= $crumb;
	}

	$trail .= $page_title;
	$trail .= '';

	return $trail;	

}

/*-----------------------------------------------------------------------------------*/
/*	Include the Rockability Framework
/*-----------------------------------------------------------------------------------*/

$tempdir = get_template_directory();
require_once($tempdir .'/framework/init.php');
require_once($tempdir .'/includes/init.php');



/*-----------------------------------------------------------------------------------*/
/*	Custom Functions
 * @author Jamaal Primus
/*-----------------------------------------------------------------------------------*/

//Contact methods for users
function my_new_contactmethods( $contactmethods ) {
// Add social media contact methods
$contactmethods['twitter'] = 'Twitter';
$contactmethods['facebook'] = 'Facebook';
$contactmethods['googleplus'] = 'Google+';
$contactmethods['linkedin'] = 'LinkedIn';
 
return $contactmethods;
}
add_filter('user_contactmethods','my_new_contactmethods',10,1);

// —————Contact methods for website settings—————–
function social_settings_api_init() {
	// Add the section to general settings so we can add our
	// fields to it
	add_settings_section('social_setting_section',
	'Social sites on the web',
	'social_setting_section_callback_function',
	'general');
	
	// Add the field with the names and function to use for our new
	// settings, put it in our new section
	add_settings_field('general_setting_facebook',
	'Facebook Page',
	'general_setting_facebook_callback_function',
	'general',
	'social_setting_section');
	
	// Register our setting so that $_POST handling is done for us and
	// our callback function just has to echo the <input>
	register_setting('general','general_setting_facebook');
	
	add_settings_field('general_setting_twitter',
	'Twitter Account',
	'general_setting_twitter_callback_function',
	'general',
	'social_setting_section');
	register_setting('general','general_setting_twitter');
	
	add_settings_field('general_setting_googleplus',
	'Google Plus Page',
	'general_setting_googleplus_callback_function',
	'general',
	'social_setting_section');
	register_setting('general','general_setting_googleplus');
	
	add_settings_field('general_setting_youtube',
	'YouTube Page',
	'general_setting_youtube_callback_function',
	'general',
	'social_setting_section');
	register_setting('general','general_setting_youtube');
	
	add_settings_field('general_setting_linkedin',
	'LinkedIn Page',
	'general_setting_linkedin_callback_function',
	'general',
	'social_setting_section');
	register_setting('general','general_setting_linkedin');
	
	add_settings_field('general_setting_email',
	'Email',
	'general_setting_email_callback_function',
	'general',
	'social_setting_section');
	register_setting('general','general_setting_email');
	
	add_settings_field('general_setting_number',
	'Number',
	'general_setting_number_callback_function',
	'general',
	'social_setting_section');
	register_setting('general','general_setting_number');
}
add_action('admin_init', 'social_settings_api_init');

// —————-Settings section callback function———————-
function social_setting_section_callback_function() {
	echo '<p>This section is where you can save the social sites where readers can find you on the Internet.</p>';
}
function general_setting_facebook_callback_function() {
	echo '<input name="general_setting_facebook" id="general_setting_facebook" type="text" value="'. get_option('general_setting_facebook') .'" />';
}
function general_setting_twitter_callback_function() {
	echo '<input name="general_setting_twitter" id="general_setting_twitter" type="text" value="'. get_option('general_setting_twitter') .'" />';
}
function general_setting_googleplus_callback_function() {
	echo '<input name="general_setting_googleplus" id="general_setting_googleplus" type="text" value="'. get_option('general_setting_googleplus') .'" />';
}
function general_setting_youtube_callback_function() {
	echo '<input name="general_setting_youtube" id="general_setting_youtube" type="text" value="'. get_option('general_setting_youtube') .'" />';
}
function general_setting_linkedin_callback_function() {
	echo '<input name="general_setting_linkedin" id="general_setting_linkedin" type="text" value="'. get_option('general_setting_linkedin') .'" />';
}
function general_setting_email_callback_function() {
	echo '<input name="general_setting_email" id="general_setting_email" type="text" value="'. get_option('general_setting_email') .'" />';
}
function general_setting_number_callback_function() {
	echo '<input name="general_setting_number" id="general_setting_number" type="text" value="'. get_option('general_setting_number') .'" />';
}

?>