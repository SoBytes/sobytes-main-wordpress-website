<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="SHIELD - Free Bootstrap 3 Theme">
    <meta name="author" content="Carlos Alvarez - Alvarez.is - blacktie.co">
    <link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/ico/favicon.png">

    <title> SHIELD - Free Bootstrap 3 Theme</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php bloginfo('template_url'); ?>/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php bloginfo('template_url'); ?>/css/main.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/icomoon.css">
    <link href="<?php bloginfo('template_url'); ?>/css/animate-custom.css" rel="stylesheet">


    
    <link href='//fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>
    
    <script src="<?php bloginfo('template_url'); ?>/js/jquery.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/modernizr.custom.js"></script>
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="<?php bloginfo('template_url'); ?>/js/html5shiv.js"></script>
      <script src="<?php bloginfo('template_url'); ?>/js/respond.min.js"></script>
    <![endif]-->
  </head>

  <body data-spy="scroll" data-offset="0" data-target="#navbar-main">
  
  
  	<div id="navbar-main">
      <!-- Fixed navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon icon-shield" style="font-size:30px; color:#3498db;"></span>
          </button>
          <a class="navbar-brand hidden-xs hidden-sm" href="#home"><span class="icon icon-shield" style="font-size:18px; color:#3498db;"></span></a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="#home" class="smoothScroll">Home</a></li>
			<li> <a href="#about" class="smoothScroll"> About</a></li>
			<li> <a href="#services" class="smoothScroll"> Services</a></li>
			<li> <a href="#team" class="smoothScroll"> Team</a></li>
			<li> <a href="#portfolio" class="smoothScroll"> Portfolio</a></li>
			<li> <a href="#blog" class="smoothScroll"> Blog</a></li>
			<li> <a href="#contact" class="smoothScroll"> Contact</a></li>
        </div><!--/.nav-collapse -->
      </div>
    </div>
    </div>
		
		<!-- ==== SECTION DIVIDER1 -->
		<section class="section-divider textdivider divider1">
			<p></p>
		</section><!-- section -->
		
		
		<!-- ==== SERVICES ==== -->
		<div class="container" id="services" name="services">
			<br>
			<br>
			<div class="row">
				<h2 class="centered">ONE BRAND, ONE VOICE.</h2>
				<hr>
				<br>
				<div class="col-lg-offset-2 col-lg-8">
					<p>Employees and consumers. Two halves of a brand’s entirety, the whole of a brand’s audience. Sometimes these two halves have very different viewpoints, creating a weak spot in the brand story. Weakness tarnishes credibility. Brands that aren’t credible aren’t viable.
					</p>
					<p>We squash weakness by designing the whole brand story. It’s crafted around the truism held by employees and consumers to create an experience that connects from the inside out.</p>
					<p>By being true to the brand we represent, we elevate the audiences’ relationship to it. Like becomes love becomes a passion. Passion becomes advocacy. And we see the brand blossom from within, creating a whole story the audience embraces. That’s when the brand can truly flex its muscles.</p>
				</div><!-- col-lg -->
			</div><!-- row -->
			
			<div class="row">
				<h2 class="centered">FEATURE IMAGE BELOW.</h2>
				<hr>
				<br>
				<div class="col-lg-offset-2 col-lg-8">
					<img class="img-responsive" src="<?php bloginfo('template_url'); ?>/img/iphone.png" alt="">
				</div><!-- col -->
			</div><!-- row -->
		</div><!-- container -->
  		

		<!-- ==== SECTION DIVIDER2 -->
		<section class="section-divider textdivider divider2">
			<div class="container">
				<h1>DESIGN IS AN INTERACTION</h1>
				<hr>
				<p>To develop a deeper and more meaningful connection with consumers, we believe design must invite them to take part in the conversation.</p>
			</div><!-- container -->
		</section><!-- section -->

	
		<!-- ==== BLOG ==== -->
		<div class="container" id="blog" name="blog">
		<br>
			<div class="row">
				<br>
				<h1 class="centered">RELATED POSTS</h1>
				<hr>
				<br>
				<br>
			</div><!-- /row -->
			
			<div class="row">
				<div class="col-lg-6 blog-bg">
					<div class="col-lg-4 centered">
					<br>
						<p><img class="img img-circle" src="<?php bloginfo('template_url'); ?>/img/team/team04.jpg" width="60px" height="60px"></p>
						<h4>Jaye Smith</h4>
						<h5>Published Aug 30.</h5>
					</div>
					<div class="col-lg-8 blog-content">
						<h2>We Define Success</h2>
						<p>Armed with insight, we embark on designing the right brand experience that engages the audience. It encompasses both the strategic direction and creative execution that solves a business problem and brings the brand to life.</p>
						<p>In the create phase, the big idea is unleashed to the world through different media touchpoints. This is when we watch the audience fall in love all over again with our client’s brand.</p>
						<p><a href="#" class="icon icon-link"> Read More</a></p>
						<br>
					</div>
				</div><!-- /col -->
				
				<div class="col-lg-6 blog-bg">
					<div class="col-lg-4 centered">
					<br>
						<p><img class="img img-circle" src="<?php bloginfo('template_url'); ?>/img/team/team03.jpg" width="60px" height="60px"></p>
						<h4>Michele Lampa</h4>
						<h5>Published Aug 28.</h5>
					</div>
					<div class="col-lg-8 blog-content">
						<h2>A Beautiful Story</h2>
						<p>Armed with insight, we embark on designing the right brand experience that engages the audience. It encompasses both the strategic direction and creative execution that solves a business problem and brings the brand to life.</p>
						<p>In the create phase, the big idea is unleashed to the world through different media touchpoints. This is when we watch the audience fall in love all over again with our client’s brand.</p>
						<p><a href="#" class="icon icon-link"> Read More</a></p>
						<br>
					</div>
				</div><!-- /col -->
			</div><!-- /row -->
			<br>
			<br>
		</div><!-- /container -->

		
		<!-- ==== SECTION DIVIDER6 ==== -->
		<section class="section-divider textdivider divider6">
			<div class="container">
				<h1>CRAFTED IN NEW YORK, USA.</h1>
				<hr>
				<p>Some Address 987,</p>
				<p>+34 9884 4893</p>
				<p><a class="icon icon-twitter" href="#"></a> | <a class="icon icon-facebook" href="#"></a></p>
			</div><!-- container -->
		</section><!-- section -->
	
		<div id="footerwrap">
			<div class="container">
				<h4>Created by <a href="http://blacktie.co">BlackTie.co</a> - Copyright 2014</h4>
			</div>
		</div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
		

	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/retina.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/smoothscroll.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery-func.js"></script>
  </body>
</html>
