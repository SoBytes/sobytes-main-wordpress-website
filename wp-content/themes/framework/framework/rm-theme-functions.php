<?php

/*-----------------------------------------------------------------------------------*/
/*  Add a rockability specific option
/*-----------------------------------------------------------------------------------*/

function rm_add_option( $name, $value ) {
    rm_update_option( $name, $value );
}


/*-----------------------------------------------------------------------------------*/
/*  Remove a rockability specific option
/*-----------------------------------------------------------------------------------*/

function rm_remove_option( $name ) {
    $rm_values = get_option('rm_framework_values');
    unset( $rm_values[$name] ); 
    update_option( 'rm_framework_values', $rm_values );
}


/*-----------------------------------------------------------------------------------*/
/*  Get a rockability specific option
/*-----------------------------------------------------------------------------------*/

function rm_get_option($name) {
    $rm_values = get_option('rm_framework_values');
    if(array_key_exists($name, $rm_values )) return $rm_values[$name];
    return false;
}


/*-----------------------------------------------------------------------------------*/
/*  Update a rockability specific option
/*-----------------------------------------------------------------------------------*/
function rm_update_option($name, $value) {
    $rm_values = get_option('rm_framework_values');
    $rm_values[$name] = $value; 
    update_option('rm_framework_values', $rm_values);
}


/*-----------------------------------------------------------------------------------*/
/*  Add metatags with Theme and Framework Versions
/*-----------------------------------------------------------------------------------*/
function rm_add_version_meta() {
    $theme_data = get_theme_data(get_template_directory() .'/style.css');

    echo '<meta name="generator" content="' . $theme_data['Name'] . ' ' . $theme_data['Version'] .'" />' . "\n";
	echo '<meta name="generator" content="rmFramework ' . rm_FRAMEWORK_VERSION . '" />' . "\n";
}

add_action('rm_meta_head', 'rm_add_version_meta');


/*-----------------------------------------------------------------------------------*/
/*  Add featured image to RSS feed
/*-----------------------------------------------------------------------------------*/

function rm_add_featured_image_to_RSS($content) {
    global $post;
    if( has_post_thumbnail($post->ID) ) {
        $content = '<div style="float:left;">' . get_the_post_thumbnail($post->ID, 'archive-thumb') . '</div>' . $content;
    }

    return $content;
}

add_filter('the_excerpt_rss', 'rm_add_featured_image_to_RSS');
add_filter('the_content_feed', 'rm_add_featured_image_to_RSS');
 

/*-----------------------------------------------------------------------------------*/
/*  Add browser detection and post name to body class
/*-----------------------------------------------------------------------------------*/

if (!function_exists('rm_browser_body_class')) {
	function rm_body_classes($classes) {
	    // Add our browser class
		global $is_lynx, $is_gecko, $is_IE, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_iphone;
	
		if($is_lynx) $classes[] = 'lynx';
		elseif($is_gecko) $classes[] = 'gecko';
		elseif($is_opera) $classes[] = 'opera';
		elseif($is_NS4) $classes[] = 'ns4';
		elseif($is_safari) $classes[] = 'safari';
		elseif($is_chrome) $classes[] = 'chrome';
		elseif($is_IE){ 
			$classes[] = 'ie';
			if(preg_match('/MSIE ([0-9]+)([a-zA-Z0-9.]+)/', $_SERVER['HTTP_USER_AGENT'], $browser_version)) $classes[] = 'ie'.$browser_version[1];
		} else $classes[] = 'unknown';
	
		if($is_iphone) $classes[] = 'iphone';
		
		// Add the post title
		if( is_singular() ) {
    		global $post;
    		array_push( $classes, "{$post->post_type}-{$post->post_name}" );
    	}
    	
    	// Add 'rm'
    	array_push( $classes, "rm" );
    	
		return $classes;
	}
}

add_filter('body_class','rm_body_classes');


/*-----------------------------------------------------------------------------------*/
/*  Get cat ID from cat name
/*-----------------------------------------------------------------------------------*/

if (!function_exists('get_category_id')) {
	function get_category_id($cat_name) {
		$term = get_term_by( 'name', $cat_name, 'category' );
		return $term->term_id;
	}
}


/*-----------------------------------------------------------------------------------*/
/*  Get "blog" URL
/*-----------------------------------------------------------------------------------*/
if (!function_exists('rm_blog_url')) {
    function rm_blog_url() {
        if( $posts_page_id = get_option('page_for_posts') ){
            return home_url(get_page_uri($posts_page_id));
        } else {
            return home_url();
        }
    }
}


/*-----------------------------------------------------------------------------------*/
/*  Get Google Webfonts
/*-----------------------------------------------------------------------------------*/

if (!function_exists('rm_get_google_fonts')) {
	function rm_get_google_fonts() {
		// Some settings
		$fonts_url  = 'http://rockablemedia.com/google-web-fonts/get.php';
		$cache_file = RM_DIR . '/cache/google-web-fonts.txt';
		$cache_time = 60 * 60 * 24 * 7;

		$cache_file_created = @file_exists($cache_file) ? @filemtime($cache_file) : 0;

		// Make sure curl is enabled
		if(is_callable('curl_init') && ini_get('allow_url_fopen')) {
			// Update only once a week
			if(time() - $cache_time > $cache_file_created) {
				// Fetch fonts
				$ch = curl_init();
				curl_setopt( $ch, CURLOPT_URL, $fonts_url );
				curl_setopt( $ch, CURLOPT_HEADER, 0 );
				curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
				$data = curl_exec( $ch );
				curl_close( $ch );

				// Update cache file
				$file = fopen( $cache_file , 'w');
				fwrite( $file, $data );
				fclose( $file );
			}
		}

		$fonts = unserialize(@file_get_contents($cache_file));
		return $fonts;
	}
}

/*-----------------------------------------------------------------------------------*/
/*	Filters that allow shortcodes in Text Widgets
/*-----------------------------------------------------------------------------------*/

add_filter('widget_text', 'shortcode_unautop');
add_filter('widget_text', 'do_shortcode');


/*-----------------------------------------------------------------------------------*/
/*	Remove Generator for Security
/*-----------------------------------------------------------------------------------*/

remove_action( 'wp_head', 'wp_generator' );
