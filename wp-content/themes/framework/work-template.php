<?php
	/*
	 Template Name: Work page template
	 */
	get_header();
?>
<!-- ==== SECTION DIVIDER2 -->
	<section class="section-divider textdivider divider1">
		<div class="container">
			<h1 class="centered">Our Work</h1>
			<hr>
			<p>Have a look at some of our latest projects</p>
		</div><!-- container -->
	</section><!-- section -->
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<?php the_content(); ?>
<?php endwhile; else : ?>
	<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>		
<?php get_footer(); ?>