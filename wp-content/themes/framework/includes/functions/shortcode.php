<?php 

/*-----------------------------------------------------------------------------------*/
/*	Shortcodes for posts
/*-----------------------------------------------------------------------------------*/

// Divider
function divider( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'header' => '',
		'para' => '',
		'image' => '',
	), $atts ) );	 
	
	// Slick sliders
	?>
	<?php if($atts['image'] == 1) : ?>
		<section class="section-divider textdivider divider1" style="background-image: url('http://sobytes.com/wp-content/uploads/2015/02/drone2.png');">
	<?php elseif($atts['image'] == 2) : ?>
		<section class="section-divider textdivider divider1" style="background-image: url('http://sobytes.com/wp-content/uploads/2015/02/divider3.jpg');">
	<?php else : ?>
		<section class="section-divider textdivider divider1" style="background-image: url('http://sobytes.com/wp-content/uploads/2015/02/divider1.jpg');">
	<?php endif; ?>
			<div class="container">
				<h1><?php echo $atts['header']?></h1>
				<hr>
				<p><?php echo $atts['para']?></p>
			</div><!-- container -->
		</section><!-- section -->
<?php
}
add_shortcode('divider','divider');

// Post with 2 columns
function post_two_colum( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'header' => '',
		'lpara' => '',
		'rpara' => '',
	), $atts ) );	 
	
	?>
	<div class="container" id="about" name="about">
		<div class="row white">
		<br>
			<h1 class="centered"><?php echo $atts['header']?></h1>
			<hr>
			
			<div class="col-lg-6">
				<p><?php echo $atts['lpara']?></p>
			</div><!-- col-lg-6 -->
			
			<div class="col-lg-6">
				<p><?php echo $atts['rpara']?></p>
			</div><!-- col-lg-6 -->
		</div><!-- row -->
	</div><!-- container -->
<?php
}
add_shortcode('post_two_colum','post_two_colum');


// Header Carousel
function headerwrap( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'icon' => '',
		'toptext' => '',
		'bottext' => '',
	), $atts ) );	 
	
	// NOT IMPLEMENTED YET
	?>
	    <div id="headerwrap" id="home" name="home">
			<header class="clearfix">
	  		 		<h1><span class="icon <?php echo $atts['icon']?>"></span></h1>
	  		 		<p><?php echo $atts['toptext']?></p>
	  		 		<p><?php echo $atts['bottext']?></p>
	  		</header>	    
	    </div>
<?php
}
add_shortcode('headerwrap','headerwrap');

// Services 
function services( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'words' => '',
	), $atts ) );	 
	?>
		<div id="greywrap">
			<div class="row" id="services">
				<?php $loop = new WP_Query( array( 'post_type' => 'Service', '3' => -1 ) ); 
					  while ( $loop->have_posts() ) : $loop->the_post(); 
					  
					  $icon = get_post_meta( get_the_ID(), '_rm_service_icon', true );
					  $short_text = wp_trim_words( get_the_content(), $num_words = $atts['words'], $more = '… ' ); ?>
				<div class="col-lg-4 callout">
					
					<h2><?php echo get_the_title(); ?></h2>
					<p><?php echo $short_text ?></p>
				</div><!-- col-lg-4 -->
			<?php endwhile; wp_reset_query(); ?>
			</div><!-- row -->
		</div><!-- greywrap -->	

<?php
}
add_shortcode('services','services');	

// Post with single column
function post_single_column( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'header' => '',
		'para' => '',
	), $atts ) );	 
	
	// POST INFO
	?>
	<!-- HTML GOES HERE -->
	<!-- ==== SERVICES ==== -->
		<div class="container" id="services" name="services">
			<br>
			<br>
			<div class="row">
				<h2 class="centered"><?php echo $atts['header']?></h2>
				<hr>
				<br>
				<div class="col-lg-offset-2 col-lg-8">
					<p><?php echo $atts['para']?>
					</p>
					</div><!-- col-lg -->
			</div><!-- row -->
			
			
<?php
}
add_shortcode('post_single_column','post_single_column');	

function image_with_title( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'header' => '',
		'image' => '',
	), $atts ) );	 
	
	// POST INFO
	?>
	<!-- HTML GOES HERE -->
	<div class="row">
				<h2 class="centered"><?php echo $atts['header']?></h2>
				<hr>
				<br>
				<div class="col-lg-offset-2 col-lg-8">
					<a href="https://s3bubble.com/" target="_blank"><img class="img-responsive" src="<?php echo $atts['image']?>" alt="SoBytes New App"></a>
				</div><!-- col -->
			</div><!-- row -->
		</div><!-- container -->
<?php
}
add_shortcode('image_with_title','image_with_title');		

// Team
function team( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'header' => '',
		'member' => '',
		'text' => '',
		'words' => '',
	), $atts ) );	 
	
	
	?>
	<!-- ==== TEAM MEMBERS ==== -->
	<div class="container" id="team" name="team">
	<br>
		<div class="row white centered">	
			<h1 class="centered"><?php echo $atts['header']?></h1>
				<hr>
				<br>
				<br>
				<?php $loop = new WP_Query( array( 'post_type' => 'Team', '4' => -1 ) ); 
					  while ( $loop->have_posts() ) : $loop->the_post();					  
					  // Get our post meta values
					  $facebook_url = get_post_meta( get_the_ID(), '_rm_service_facebook', true );
					  $twitter_url = get_post_meta( get_the_ID(), '_rm_service_twitter', true );
				      $flickr_url = get_post_meta( get_the_ID(), '_rm_service_flickr', true );?>

						<div class="col-lg-3 centered">
							<?php 
							if ( has_post_thumbnail() ) { 
								the_post_thumbnail( 'team-home-image', array('class' => 'img team', 'alt' => get_the_title()) ); 
							}
							?>
							<br>
							<h4><b><?php echo get_the_title(); ?></b></h4>
							<p><?php echo get_the_content(); ?></p>
						</div><!-- col-lg-3 -->
				<?php endwhile; wp_reset_query(); ?>
			</div>
	</div><!-- container -->
<?php
}
add_shortcode('team','team');	
	
	
function image_text_button( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'header' => '',
		'text' => '',
		'buttontext' => '',
		'link' => ''
	), $atts ) );	 
	
	// POST INFO
	?>
	<!-- ==== GREYWRAP ==== -->
		<div id="greywrap">
			<div class="container">
					<div class="row">
						<div class="col-lg-8 centered">
							<img class="img-responsive" src="http://sobytes.com/wp-content/uploads/2015/02/macbook1.png" align="SoBytes latest project The CDF"> <!-- NEED TO CHANGE -->
						</div>
						<div class="col-lg-4">
							<h2><?php echo $atts['header']?></h2>
							<p><?php echo $atts['text']?></p>
							<p><a class="btn btn-success" href="https://play.google.com/store/apps/details?id=com.cdf.live" target="_blank"><i class="icon icon-mobile"></i> Android</a> <a class="btn btn-success" href="https://itunes.apple.com/us/app/the-cdf/id965141337?ls=1&mt=8" target="_blank"><i class="icon icon-mobile"></i> IOS Apple</a></p>
						</div>					
					</div><!-- row -->
			</div>
			<br>
			<br>
		</div><!-- greywrap -->

<?php
}
add_shortcode('image_text_button','image_text_button');	

// Portfolio
function portfolio( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'header' => '',
		'words' => '',
	), $atts ) );	 
	
	// POST INFO
	?>
		<div class="container" id="portfolio" name="portfolio">
		<br>
			<div class="row">
				<br>
				<?php if($atts['header']) : ?>
					<h1 class="centered"><?php echo $atts['header']?></h1>
					<hr>
				<?php endif; ?>
				<br>
				<br>
			</div><!-- /row -->
			<div class="row">	
			<?php $loop = new WP_Query( array( 'post_type' => 'Project', 'posts_per_page' => 6 ) ); 
					  while ( $loop->have_posts() ) : $loop->the_post(); 
					    $title = wp_trim_words( get_the_title(), $num_words = 2, $more = '… ' )
					  ?>
				<!-- PORTFOLIO IMAGE -->
				<div class="col-md-4 ">
			    	<div class="grid mask">
						<figure>
							<a href="<?php the_permalink(); ?>"><?php 
							if ( has_post_thumbnail() ) { 
								the_post_thumbnail( 'work-image', array('class' => 'img-responsive') ); 
							}
							?></a>
							<figcaption>
								<h5><?php echo $title; ?></h5>
								<a href="<?php the_permalink(); ?>" class="btn">Read More</a>
							</figcaption><!-- /figcaption -->
						</figure><!-- /figure -->
			    	</div><!-- /grid-mask -->
				</div><!-- /col -->
					 <!-- MODAL SHOW THE PORTFOLIO IMAGE. In this demo, all links point to this modal. You should create a modal for each of your projects. -->
					  <div class="modal fade" id="<?php echo get_the_title() . "modal"; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					    <div class="modal-dialog">
					      <div class="modal-content">
					        <div class="modal-header">
					          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					          <h4 class="modal-title"><?php echo get_the_title(); ?></h4>
					        </div>
					        <div class="modal-body">
					          <p><img class="img-responsive" src="<?php echo $feat_image ?>" alt=""></p>
					          <p><?php echo wp_trim_words( get_the_content(), $num_words = $atts['words'], $more = '… ' ); ?></p>
					          <p><b><a href="<?php echo get_permalink(); ?>">Visit Page</a></b></p>
					        </div>
					        <div class="modal-footer">
					          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					        </div>
					      </div><!-- /.modal-content -->
					    </div><!-- /.modal-dialog -->
					  </div><!-- /.modal -->
				<?php endwhile; wp_reset_query(); ?>
			</div><!-- /row -->
			<br>
			<br>
		</div><!-- /row -->
	</div><!-- /container -->
<?php
}
add_shortcode('portfolio','portfolio');	


// Blog
function blog( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'header' => '',
		'numposts' => '',
		'words' => '',
	), $atts ) );	 
	
	// POST INFO
	?>
	<!-- ==== BLOG ==== -->
	<div class="container" id="blog" name="blog">
	<br>
		<div class="row">
			<br>
			<h1 class="centered"><?php echo $atts['header']?></h1>
			<hr>
			<br>
			<br>
		</div><!-- /row -->
		<div class="row">
			<?php 
			$args = array(
			    'post_type' => 'post',
			    'orderby' => 'date',
			    'order' => 'DESC',
			    'posts_per_page' => 2
			);
			$num_posts = $plugin_args['numposts'];
			$loop = new WP_Query( $args ); 
					  while ( $loop->have_posts() ) : $loop->the_post();
				      $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); 
				      $author = get_the_author(); 
					  $cat = get_the_category( $post->ID );
				      $my_date = get_the_date('M d',$post->ID); 
			          $shorttitle = wp_trim_words( get_the_title(), $num_words = 6, $more = '… ' );
				      $shortexcerpt = wp_trim_words( strip_shortcodes(get_the_content()), $num_words = 20, $more = '… ' );
				      ?>
			<div class="col-lg-6 blog-bg">
				<div class="col-lg-4 centered">
				<br>
					<p><?php echo get_wp_user_avatar(get_the_author_meta('ID'), 100); ?></p>
					<h4><?php echo $author ?></h4>
					<h5><?php echo $cat[0]->name; ?></h5>
				</div>
				<div class="col-lg-8 blog-content">
					<h2 class="blog-short-header"><a href="<?php echo get_permalink(); ?>"><?php echo $shorttitle; ?></a></h2>
					<p><?php echo $shortexcerpt; ?></p>
					<p><a href="<?php echo get_permalink(); ?>" class="icon icon-link"> Read More</a></p>
					<br>
				</div>
			</div><!-- /col -->
			
			<?php endwhile; wp_reset_query(); ?>
		</div><!-- /row -->
		<br>
		<br>
	</div><!-- /container -->
<?php
}
add_shortcode('blog','blog');	
	
?>