<?php

//	Home slider custom post type
function home_slider_post_type() {
  $labels = array(
    'name'               => _x( 'Home Slider', 'post type general name' ),
    'singular_name'      => _x( 'Home Slider', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'Slider' ),
    'add_new_item'       => __( 'Add New Slide' ),
    'edit_item'          => __( 'Edit Slide' ),
    'new_item'           => __( 'New Slide' ),
    'all_items'          => __( 'All Slide' ),
    'view_item'          => __( 'View Slide' ),
    'search_items'       => __( 'Search Slide' ),
    'not_found'          => __( 'No Slide found' ),
    'not_found_in_trash' => __( 'No Slide found in the Trash' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Home Slider'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Holds our Slides fro home slider',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' ),
    'has_archive'   => true,
  );
  register_post_type( 'HomeSlider', $args ); 
}
add_action( 'init', 'home_slider_post_type' );

//	Service post type
function service_post_type() {
  $labels = array(
    'name'               => _x( 'Service', 'post type general name' ),
    'singular_name'      => _x( 'Service', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'service' ),
    'add_new_item'       => __( 'Add New Service' ),
    'edit_item'          => __( 'Edit Service' ),
    'new_item'           => __( 'New Service' ),
    'all_items'          => __( 'All Service' ),
    'view_item'          => __( 'View Service' ),
    'search_items'       => __( 'Search Service' ),
    'not_found'          => __( 'No Service found' ),
    'not_found_in_trash' => __( 'No Service found in the Trash' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Service'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Holds our Service and Service specific data',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' ),
    'has_archive'   => true,
  );
  register_post_type( 'Service', $args ); 
}
add_action( 'init', 'service_post_type' );

//	Team post type
function team_post_type() {
  $labels = array(
    'name'               => _x( 'Team', 'post type general name' ),
    'singular_name'      => _x( 'Team', 'post type singular name' ),
    'add_new'            => _x( 'Add New Member', 'service' ),
    'add_new_item'       => __( 'Add New Team member' ),
    'edit_item'          => __( 'Edit Team Member' ),
    'new_item'           => __( 'New Team Member' ),
    'all_items'          => __( 'All Team Members' ),
    'view_item'          => __( 'View Team Members' ),
    'search_items'       => __( 'Search Team Members' ),
    'not_found'          => __( 'No Team found' ),
    'not_found_in_trash' => __( 'No Team found in the Trash' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Team'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Holds our Team and Team specific data',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' ),
    'has_archive'   => true,
  );
  register_post_type( 'Team', $args ); 
}
add_action( 'init', 'team_post_type' );

//	Project post type
function project_post_type() {
  $labels = array(
    'name'               => _x( 'Project', 'post type general name' ),
    'singular_name'      => _x( 'Project', 'post type singular name' ),
    'add_new'            => _x( 'Add New Project', 'service' ),
    'add_new_item'       => __( 'Add New Project' ),
    'edit_item'          => __( 'Edit Project' ),
    'new_item'           => __( 'New Project' ),
    'all_items'          => __( 'All Projects' ),
    'view_item'          => __( 'View Projects' ),
    'search_items'       => __( 'Search Projects' ),
    'not_found'          => __( 'No Project found' ),
    'not_found_in_trash' => __( 'No Project found in the Trash' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Project'
  );
  $args = array(
    'labels'        => $labels,
    'public' => true,
    'menu_position' => 15,
    'supports' => array( 'title', 'editor', 'comments', 'thumbnail', 'custom-fields' ),
    'taxonomies' => array( '' ),
    'has_archive' => true
  );
  register_post_type( 'project', $args ); 
}
add_action( 'init', 'project_post_type' );

function include_project_template_function( $template_path ) {
    if ( get_post_type() == 'project' ) {
        if ( is_single() ) {
            // checks if the file exists in the theme first,
            // otherwise serve the file from the plugin
            if ( $theme_file = locate_template( array ( 'single-project.php' ) ) ) {
                $template_path = $theme_file;
            } else {
                $template_path = plugin_dir_path( __FILE__ ) . '/single-project.php';
            }
        }
    }
    return $template_path;
}

add_filter( 'template_include', 'include_project_template_function', 1 );

?>