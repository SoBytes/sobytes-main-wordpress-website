<?php

/**
 * Create the typography section
 */
add_action('admin_init', 'rm_typography_settings');

function rm_typography_settings(){
    $typography_settings['description'] = 'Control and configure the typography settings of your theme.';

    $typography_settings[] = array('title' => 'Enable Custom Typography',
                               'desc' => 'Enable typography Columns.',
                               'type' => 'checkbox',
                               'id' => 'typography_enable',
                               );

    $typography_settings[] = array('title' => 'General Typography',
                               'desc' => 'The main typography throughout your website.',
                               'type' => 'typography',
                               'id' => 'typography_general',
                               );

    $typography_settings[] = array('title' => 'Navigation',
                               'desc' => 'The typography set for navigation elements.',
                               'type' => 'typography',
                               'id' => 'typography_navigation',
                               );

    $typography_settings[] = array('title' => 'Page Titles',
                               'desc' => 'The typography set for page title.',
                               'type' => 'typography',
                               'id' => 'typography_page_titles',
                               );

    $typography_settings[] = array('title' => 'Post Titles',
                               'desc' => 'The typography set for post title.',
                               'type' => 'typography',
                               'id' => 'typography_post_titles',
                               );

    $typography_settings[] = array('title' => 'Post Meta',
                               'desc' => 'The typography set for post meta information.',
                               'type' => 'typography',
                               'id' => 'typography_post_meta',
                               );

    $typography_settings[] = array('title' => 'Widget Titles',
                               'desc' => 'The typography set for sidebar widget titles.',
                               'type' => 'typography',
                               'id' => 'typography_widget_title',
                               );
                                
    rm_add_framework_page( 'Typography', $typography_settings, 15 );
}

?>