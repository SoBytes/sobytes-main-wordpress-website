<?php

/**
 * Create the Styling Options section
 */
add_action('admin_init', 'rm_styling_options');

function rm_styling_options(){

  // Google Fonts
  $google_fonts = array_merge(
    array( '' => __('None / Default', 'rm') ),
    rm_get_google_fonts()
  );
	
	$styling_options['description'] = 'Configure the visual appearance of you theme by selecting a stylesheet if applicable, choosing your overall layout and inserting any custom CSS necessary.';

    $styling_options[] = array('title' => 'Background Colour',
                               'desc' => 'Set the website background colour.',
                               'type' => 'colour',
                               'id' => 'style_background_image',
                               );

    $styling_options[] = array('title' => 'Background Image',
                               'desc' => 'Set the website background image.',
                               'type' => 'file',
                               'id' => 'style_background_image',
                               );

    $styling_options[] = array('title' => 'Background Repeat',
                               'desc' => 'Set the website background image.',
                               'type' => 'select',
                               'id' => 'style_background_repeat',
                               'options' => array(
                                  'no-repeat' => 'No Repeat',
                                  'repeat-y' => 'Repeat Vertically',
                                  'repeat-x' => 'Repeat Horizonally',
                                ));

    $styling_options[] = array('title' => 'Background Position',
                               'desc' => 'Set the website background position.',
                               'type' => 'select',
                               'id' => 'style_background_position',
                               'options' => array(
                                  'top left' => 'Top Left',
                                  'top center' => 'Top Center',
                                  'top right' => 'Top Right',
                                  'center left' => 'Center Left',
                                  'center center' => 'Center Center',
                                  'center right' => 'Center Right',
                                  'bottom left' => 'Bottom Left',
                                  'bottom bottom' => 'Bottom Center',
                                  'bottom right' => 'Bottom Right',
                                ));

    $styling_options[] = array('title' => 'Background Attachment',
                               'desc' => 'Set the website background attachment.',
                               'type' => 'select',
                               'id' => 'style_background_attachment',
                               'options' => array(
                                  'scroll' => 'Scroll',
                                  'fixed' => 'Fixed'
                                ));

    $styling_options[] = array('title' => 'Custom CSS',
                               'desc' => 'Quickly add some CSS to your theme by adding it to this block.',
                               'type' => 'textarea',
                               'id' => 'style_custom_css');
                                
    rm_add_framework_page( 'Styles', $styling_options, 10 );
}


/**
 * Output main layout
 */
function rm_style_main_layout($classes){
	$rm_values = get_option( 'rm_framework_values' );
	$layout = 'layout-2cr';
	if( array_key_exists( 'style_main_layout', $rm_values ) && $rm_values['style_main_layout'] != '' ){
		$layout = $rm_values['style_main_layout'];
	}
	$classes[] = $layout;
	return $classes;
}
add_filter( 'body_class', 'rm_style_main_layout' );


/**
 * Output the custom CSS
 */
function rm_custom_css($content) {
    $rm_values = get_option( 'rm_framework_values' );
    if( array_key_exists( 'style_custom_css', $rm_values ) && $rm_values['style_custom_css'] != '' ){
    	$content .= '/* Custom CSS */' . "\n";
        $content .= stripslashes($rm_values['style_custom_css']);
        $content .= "\n\n";
    }
    return $content;
    
}
add_filter( 'rm_custom_styles', 'rm_custom_css' );

?>