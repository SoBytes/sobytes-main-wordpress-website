<?php

/**
 * Create the Settings section
 */
add_action('admin_init', 'rm_settings_options');

function rm_settings_options(){
	$settings_options['description'] = 'Configure your settings';

    $settings_options[] = array('title' => 'Settings',
                                'desc' => 'Settings here.',
                                'type' => 'text',
                                'id' => 'settings');
                                
    rm_add_framework_page( 'Settings ', $settings_options, 35 );
}



?>