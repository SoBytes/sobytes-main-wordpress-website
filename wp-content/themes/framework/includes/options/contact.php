<?php

/**
 * Create the Contact and Social section
 */
add_action('admin_init', 'rm_contact_options');

function rm_contact_options(){
	$contact_options['description'] = 'Configure your contact and social settings';

    $contact_options[] = array('title' => 'Contact Form Email Address',
                                'desc' => 'Enter the email address where you\'d like to receive emails from the contact form, or leave blank to use admin email.',
                                'type' => 'text',
                                'id' => 'contact_contact_email');

    $contact_options[] = array('title' => 'Facebook URL',
                                'desc' => 'Enter your Facebook URL (Please include  http://:)',
                                'type' => 'text',
                                'id' => 'contact_facebook');

    $contact_options[] = array('title' => 'Twitter Username',
                                'desc' => 'Enter your Twitter username',
                                'type' => 'text',
                                'id' => 'contact_twitter');

    $contact_options[] = array('title' => 'Youtube URL',
                                'desc' => 'Enter your Youtube URL (Please include  http://:)',
                                'type' => 'text',
                                'id' => 'contact_youtube');
								
    $contact_options[] = array('title' => 'Google Plus URL',
                                'desc' => 'Enter your Google Plus URL (Please include  http://:)',
                                'type' => 'text',
                                'id' => 'contact_googleplus');

    $contact_options[] = array('title' => 'Pinterest URL',
                                'desc' => 'Enter your Pinterest URL (Please include  http://:)',
                                'type' => 'text',
                                'id' => 'contact_pinterest');
								
    $contact_options[] = array('title' => 'Dribble URL',
                                'desc' => 'Enter your Dribble URL (Please include  http://:)',
                                'type' => 'text',
                                'id' => 'contact_dribble');
								
    $contact_options[] = array('title' => 'LinkedIn URL',
                                'desc' => 'Enter your LinkedIn URL (Please include  http://:)',
                                'type' => 'text',
                                'id' => 'contact_linkedin');
								
	$contact_options[] = array('title' => 'Flickr URL',
                                'desc' => 'Enter your Flickr URL (Please include  http://:)',
                                'type' => 'text',
                                'id' => 'contact_flickr');

	$contact_options[] = array('title' => 'Tumblr URL',
                                'desc' => 'Enter your Tumblr URL (Please include  http://:)',
                                'type' => 'text',
                                'id' => 'contact_tumblr');

	$contact_options[] = array('title' => 'Instagram URL',
                                'desc' => 'Enter your Instagram URL (Please include  http://:)',
                                'type' => 'text',
                                'id' => 'contact_instagram');

	$contact_options[] = array('title' => 'Vimeo URL',
                                'desc' => 'Enter your Vimeo URL (Please include  http://:)',
                                'type' => 'text',
                                'id' => 'contact_vimeo');
								
	$contact_options[] = array('title' => 'Email',
                                'desc' => 'Enter your email',
                                'type' => 'text',
                                'id' => 'contact_email');
                                
	$contact_options[] = array('title' => 'Phone',
                                'desc' => 'Enter your phone number',
                                'type' => 'text',
                                'id' => 'contact_phone_number');
								
	$contact_options[] = array('title' => 'Skype',
                                'desc' => 'Enter your Skype username',
                                'type' => 'text',
                                'id' => 'contact_skype_name');
								
    rm_add_framework_page( 'Contact ', $contact_options, 30 );
}

?>