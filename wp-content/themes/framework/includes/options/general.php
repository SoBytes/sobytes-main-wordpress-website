<?php

/**
 * Create the General Settings section
 */
add_action('admin_init', 'rm_general_settings');

function rm_general_settings(){
    $general_settings['description'] = 'Control and configure the general setup of your theme.';

    $general_settings[] = array('title' => 'Text Title',
                                'desc' => 'Use your website title as your logo.',
                                'type' => 'checkbox',
                                'id' => 'general_text_title',
                                'val' => 'Upload Image');
                                
	$general_settings[] = array('title' => 'Custom Logo Upload',
                                'desc' => 'Upload a logo for your theme.',
                                'type' => 'file',
                                'id' => 'general_custom_logo',
                                'val' => 'Upload Image');
                                
    $general_settings[] = array('title' => 'Custom Favicon Upload',
                                'desc' => 'Upload a 16px x 16px Png/Gif image that will represent your website\'s favicon.',
                                'type' => 'file',
                                'id' => 'general_custom_favicon',
                                'val' => 'Upload Image');
                                
    rm_add_framework_page( 'General', $general_settings, 5 );
}


/**
 * Output the favicon
 */
function rm_custom_favicon() {
    $rm_values = get_option( 'rm_framework_values' );
    if( array_key_exists( 'general_custom_favicon', $rm_values ) && $rm_values['general_custom_favicon'] != '' )
        echo '<link rel="shortcut icon" href="'. $rm_values['general_custom_favicon'] .'" />' . "\n";
}
add_action( 'wp_head', 'rm_custom_favicon' );

/**
 * Redirect the RSS feed
 * Credit: Feedburner Feedsmith Plugin
 */
if( !preg_match("/feedburner|feedvalidator/i", $_SERVER['HTTP_USER_AGENT']) ){
	add_action( 'template_redirect', 'rm_feed_redirect' );
	add_action( 'init', 'rm_check_url' );
}
function rm_feed_redirect() {
	global $wp, $feed, $withcomments;
    
    $rm_values = get_option( 'rm_framework_values' );
    if( array_key_exists( 'general_feedburner_url', $rm_values ) && $rm_values['general_feedburner_url'] != '' ){
        if( is_feed() && $feed != 'comments-rss2' && !is_single() && $wp->query_vars['category_name'] == '' && ($withcomments != 1) ){
            if( function_exists('status_header') ) status_header( 302 );
            header("Location:" . trim($rm_values['general_feedburner_url']));
            header("HTTP/1.1 302 Temporary Redirect");
            exit();
        }
    }
}
function rm_check_url() {
	$rm_values = get_option( 'rm_framework_values' );
    if( array_key_exists( 'general_feedburner_url', $rm_values ) && $rm_values['general_feedburner_url'] != '' ){
        switch( basename($_SERVER['PHP_SELF']) ){
            case 'wp-rss.php':
            case 'wp-rss2.php':
            case 'wp-atom.php':
            case 'wp-rdf.php':
                if( function_exists('status_header') ) status_header( 302 );
                header("Location:" . trim($rm_values['general_feedburner_url']));
                header("HTTP/1.1 302 Temporary Redirect");
                exit();
                break;
        }
    }
}

?>