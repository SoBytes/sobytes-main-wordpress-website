<?php

/**
 * Create the Layout section
 */
add_action('admin_init', 'rm_layout_settings');

function rm_layout_settings(){
    $layout_settings['description'] = 'Control and configure the layout settings of your theme.';
                                
    $layout_settings[] = array('title' => 'Main Layout',
                               'desc' => 'Select main content and sidebar alignment. This option can be over written within individual pages.',
                               'type' => 'radio',
                               'id' => 'layout_main_layout',
                               'val' => 'layout-2cr',
                               'options' => array(
                                    'layout-full' => 'Full Width',
                                    'layout-2cl' => '2 Columns (left)',
                                    'layout-2cr' => '2 Columns (right)'
                               ));
                                
    rm_add_framework_page( 'Layout', $layout_settings, 20 );
}

?>