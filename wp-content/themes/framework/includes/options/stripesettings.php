<?php

/**
 * Create the Settings section
 */
add_action('admin_init', 'rm_stripe_settings_options');

function rm_stripe_settings_options(){
	global $stripe_settings_options;
	
	$stripe_settings_options['description'] = 'Configure your stripe settings';
	
	$stripe_settings_options[] = array('title' => 'Enable Test Mode',
                                'desc' => 'Enable/Disable Test Mode',
                                'type' => 'checkbox',
                                'id' => 'test_mode');

    $stripe_settings_options[] = array('title' => 'Stripe Secret Key',
                                'desc' => 'Secret Key.',
                                'type' => 'text',
                                'id' => 'live_secret_key');
								
    $stripe_settings_options[] = array('title' => 'Stripe Publishable Key',
                                'desc' => 'Publishable Key.',
                                'type' => 'text',
                                'id' => 'live_publishable_key');
								
    $stripe_settings_options[] = array('title' => 'Stripe Test Secret Key',
                                'desc' => 'Test Secret Key.',
                                'type' => 'text',
                                'id' => 'test_secret_key');
								
    $stripe_settings_options[] = array('title' => 'Stripe Test Publishable Key',
                                'desc' => 'Test Publishable Key.',
                                'type' => 'text',
                                'id' => 'test_publishable_key');
								
	$stripe_settings_options[] = array('title' => 'Currency',
                                'desc' => 'Currency you want to use. Please use "gbp" or "usd"',
                                'type' => 'text',
                                'id' => 'currency');
												                         
    rm_add_framework_page( 'Stripe Payment Settings', $stripe_settings_options, 40 );
}