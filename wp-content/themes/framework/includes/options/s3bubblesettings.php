<?php

/**
 * Create the Settings section
 */
add_action('admin_init', 'rm_s3bubble_settings_options');

function rm_s3bubble_settings_options(){
	
	$s3_settings_options['description'] = 'Configure your S3Bubble settings';

    $s3_settings_options[] = array('title' => 'S3Bubble Access Key:',
                                'desc' => 'Access Key.',
                                'type' => 'text',
                                'id' => 'S3Bubble');
								
    $s3_settings_options[] = array('title' => 'S3Bubble App Secret Key:',
                                'desc' => 'Secret Key.',
                                'type' => 'text',
                                'id' => 'S3Bubble');
	
	$s3_settings_options[] = array('title' => '',
                                'desc' => '',
                                'type' => 'spacer',
                                'id' => 'spacer');
												                         
    rm_add_framework_page( 'S3Bubble Settings ', $s3_settings_options, 45 );
}

?>