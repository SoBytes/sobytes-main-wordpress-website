<?php

/**
 * Create the footer section
 */
add_action('admin_init', 'rm_footer_settings');

function rm_footer_settings(){
    $footer_settings['description'] = 'Control and configure the footer settings of your theme.';

    $footer_settings[] = array('title' => 'Enable Custom Footer',
                               'desc' => 'Enable Footer Columns.',
                               'type' => 'checkbox',
                               'id' => 'footer_enable',
                               );
                                
    $footer_settings[] = array('title' => 'Footer Layout',
                               'desc' => 'Select the general footer layout.',
                               'type' => 'radio',
                               'id' => 'footer_main_layout',
                               'val' => 'footer-full',
                               'options' => array(
                                    'footer-full' => 'Full Width',
                                    'footer-2c' => '2 Columns',
                                    'footer-3c' => '3 Columns',
                                    'footer-4c' => '4 Columns',
                               ));

    $footer_settings[] = array('title' => 'Footer Copyright',
                               'desc' => 'This text will appear as the copyright within the footer.',
                               'type' => 'textarea',
                               'id' => 'footer_main_layout',
                               );
                                
    rm_add_framework_page( 'Footer', $footer_settings, 25 );
}

?>