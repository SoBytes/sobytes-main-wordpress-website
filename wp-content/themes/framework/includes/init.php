<?php

$incdir = get_template_directory() . '/includes/';


/*-----------------------------------------------------------------------------------*/
/*	Load Theme Specific Helpers
/*-----------------------------------------------------------------------------------*/

require_once($incdir .'helpers/wp_bootstrap_navwalker.php');

/*-----------------------------------------------------------------------------------*/
/*	Load Theme Specific Components
/*-----------------------------------------------------------------------------------*/

//require_once($incdir .'meta/post-meta.php');


/*-----------------------------------------------------------------------------------*/
/*	Load Framework Sections
/*-----------------------------------------------------------------------------------*/

require_once($incdir .'options/general.php');
require_once($incdir .'options/styles.php');
require_once($incdir .'options/typography.php');
require_once($incdir .'options/layout.php');
require_once($incdir .'options/footer.php');
require_once($incdir .'options/contact.php');
require_once($incdir .'options/settings.php');

/*-----------------------------------------------------------------------------------*/
/*	Include extra PHP files
/*-----------------------------------------------------------------------------------*/

require_once($incdir .'functions/shortcode.php');
require_once($incdir .'functions/post_types.php');

/*-----------------------------------------------------------------------------------*/
/*	Load Widgets
/*-----------------------------------------------------------------------------------*/

/* No Widgets to load */

