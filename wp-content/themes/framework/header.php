<?php
    
    flush_rewrite_rules();
    echo "HELLO";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/images/favicon.ico">
    <title><?php wp_title(); ?></title>

    <!-- Custom styles for this template -->
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/dist/css/reset.min.css"/>
    <!-- Bootstrap core CSS -->
    <link href="<?php bloginfo('template_url'); ?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php bloginfo('template_url'); ?>/css/main.min.css" rel="stylesheet">
    <link href="<?php bloginfo('template_url'); ?>/css/animate-custom.min.css" rel="stylesheet">

    <link href='//fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>
    
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/dist/js/modernizr.min.js"></script>
    
    <!-- Slick CSS -->
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/slick/slick.min.css"/>
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/slick/slick-theme.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/dist/css/slider.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/dist/css/menu.min.css"/>

    <?php wp_head(); ?>
    </head>
<body>
<header class="sobytes-menu-main-header">
  <a class="sobytes-menu-logo" href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_url'); ?>/images/sobytes-small-logo.png" alt="Logo"></a>

  <ul class="sobytes-menu-header-buttons">
    <li><a class="sobytes-menu-search-trigger" href="#sobytes-menu-search"><span></span></a></li>
    <li><a class="sobytes-menu-nav-trigger" href="#sobytes-menu-primary-nav"><span></span></a></li>
  </ul> <!-- sobytes-menu-header-buttons -->
</header>

<main class="sobytes-menu-main-content">