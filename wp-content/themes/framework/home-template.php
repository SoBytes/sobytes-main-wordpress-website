<?php
/*
 Template Name: Home page template
 */
get_header(); ?>

	<section class="sobytes-slider-hero">
		<ul class="sobytes-slider-hero-slider autoplay">
			<li class="selected">
				<div class="sobytes-slider-full-width">
					<h2>We Build Mobile Apps & Web Services</h2>
					<p>IOS, Android or Amazon Web Services Super Powered Application We Can help.</p>
					<a href="<?php bloginfo('url'); ?>/#contact" class="sobytes-slider-btn">Contact</a>
					<a href="https://itunes.apple.com/gb/artist/sobytes-ltd/id580676068" target="_blank" class="sobytes-slider-btn secondary">View All Apps</a>
				</div> <!-- .sobytes-slider-full-width -->
			</li>
			<li>
				<div class="sobytes-slider-full-width">
					<h2>Amazon Web Services</h2>
					<p>We can put your whole service in the cloud from Wordpress website or framework to all your media assets super powering your setup.</p>
					<a href="<?php bloginfo('url'); ?>/#contact" class="sobytes-slider-btn">Contact</a>
					<a href="https://s3bubble.com" class="sobytes-slider-btn secondary">Latest Project</a>
				</div> <!-- .sobytes-slider-full-width -->
			</li>
		</ul> <!-- .sobytes-slider-hero-slider -->
		<div class="sobytes-slider-slider-nav">
			<nav>
				<span class="sobytes-slider-marker item-1"></span>
				
				<ul>
					<li class="selected"><a href="#0">Welcome</a></li>
					<li><a href="#0">AWS</a></li>
				</ul>
			</nav> 
		</div> <!-- .sobytes-slider-slider-nav -->
	</section> <!-- .sobytes-slider-hero -->
	<?php
	if ( have_posts() ) : while ( have_posts() ) : the_post();
		the_content();
	endwhile; else:
	endif; ?>

<?php get_footer(); ?>