<h2>CATEGORIES</h2>
<ul class="list-group">
	<?php 
  $categories = get_categories(); 
  foreach ($categories as $category) {
  	$option = '<li class="list-group-item"><span class="badge">'.$category->category_count.'</span><a href="http://sobytes.com/category/'.$category->slug.'">';
	$option .= $category->cat_name;
	$option .= '</a></li>';
	echo $option;
  }
 ?>
</ul>